package com.Lock;

import com.Lock.com.Logic.FisherFace;
import com.Lock.com.Utilities.HibernateUtilities;
import com.ibm.watson.developer_cloud.visual_recognition.v3.VisualRecognition;
import org.bytedeco.javacpp.opencv_face;
import org.hibernate.Session;

import javax.swing.*;
/**
 *@author Jason Bell
 *
 **/
public class ProjectGui implements Runnable {
    private static Thread Guithread;
    private JButton newFaceRecogniserButton;
    private JButton newTestButton;
    private JButton quitButton;
    private JButton loadRecogniserButton;
    private JPanel panel;
    private JButton temp_creator;
    private JButton temp_tester;
    private JButton Lock;
    private FisherFace fish1;
    private opencv_face.FaceRecognizer fishface = new opencv_face.FaceRecognizer(null);
    private static VisualRecognition service;
    /**
     * Class to create the main GUI window of the project
     * Initiates the action buttons of the main GUI window
     */
    private ProjectGui(){
        newFaceRecogniserButton.addActionListener(e -> {
                System.out.println("new Trainer");
                fish1 = new FisherFace(fishface);
                fish1.createTrainer();
        });
        loadRecogniserButton.addActionListener(e -> {
                System.out.println("Loading Trainer");
                fish1 = new FisherFace(fishface);
                fish1.loadTrainer();
               // System.out.println("Trainer loaded");
        });
        newTestButton.addActionListener(e -> {
                System.out.println("New Test");
                fish1.testImage();
        });
        temp_creator.addActionListener(e ->  {
                System.out.println("creating new temporary user");
                fish1 = new FisherFace(fishface);
                fish1.createTempTrainer();
        });
        temp_tester.addActionListener(e ->  {
                System.out.println("Performing test on temp user");
                fish1.tempTest();
        });
        quitButton.addActionListener(e -> System.exit(0));

        Lock.addActionListener(e->FisherFace.locker());}

    /**
     * Thread to run the main GUI from
     */
    public void run(){
        JFrame frame = new JFrame("ProjectGui");
        ProjectGui newGUI = new ProjectGui();
        newGUI.createGui(frame);
    }

    /**
     * Creates the main GUI frame and sets the buttons on it
     * @param frame A frame object to pack the GUI into
     */
    private void createGui(JFrame frame){
        frame.setContentPane(new ProjectGui().panel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    public static VisualRecognition getService(){
        return service;
    }
    /**
     * Main method to start the program
     * @param args Argument of strings
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
                Guithread = new Thread(new ProjectGui());
                Guithread.start();
        });
        Session session = HibernateUtilities.getSession();
        service = new VisualRecognition(VisualRecognition.VERSION_DATE_2016_05_20);
        service.setApiKey("************************************");//No longer have access to key. Face detection system needs a new method.
    }
}