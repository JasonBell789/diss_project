/*
public class Tester {
    private static void ORLTester(FisherFace recognizer) {
        int counter = 0;
        File folder = new File("C:\\Users\\jason\\IdeaProjects\\diss_project\\src\\resources\\Chosen");
        File[] listOfFiles = folder.listFiles();
        try{
        for (int i = 0; i < 41; i++) {
            IntPointer label = new IntPointer(1);
            DoublePointer confidence = new DoublePointer(1);

            opencv_core.Mat tester = imread(listOfFiles[i].getPath(), CV_LOAD_IMAGE_GRAYSCALE);
            recognizer.getFish().predict(tester, label, confidence);
            int predictedLabel = label.get(0);
            double predictedConfidence = confidence.get(0);
            System.out.println("Image number :"+i+"Predicted label: " + predictedLabel + " " +
                    "Predicted Confidence: " + predictedConfidence);
            if (predictedConfidence <450){
                counter++;
            }
            System.out.print(counter);
        }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private static FisherFace recogniser;
    private static opencv_face.FaceRecognizer fish;
    public static void main(String[]args){
        FilenameFilter imageFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                name = name.toLowerCase();
                return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
            }
        };
        File location = new File("Profile");
        File[] imageArray = location.listFiles(imageFilter);

        //crops the images
        int counter = 0;
        if (imageArray != null) {
            for (File image : imageArray) {
                opencv_core.Mat testImage = imread(image.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
                opencv_core.Mat newImage = new opencv_core.Mat();
                resize(testImage,newImage,new opencv_core.Size(92,112));
                imwrite("Profile//" + counter + "-testImage.jpg", newImage);
                counter++;
            }
            imageArray = location.listFiles(imageFilter);
            if(imageArray !=null) {
                opencv_core.MatVector images = new opencv_core.MatVector(imageArray.length);
                opencv_core.Mat labels = new opencv_core.Mat(imageArray.length, 1, CV_32SC1);

                IntBuffer labelsBuffer = labels.createBuffer();
                counter = 0;

                for (File image : imageArray) {
                    opencv_core.Mat imge = imread(image.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
                    int label = Integer.parseInt(image.getName().split("-")[0]);
                    images.put(counter, imge);
                    labelsBuffer.put(counter, label);
                    counter++;
                }
                if(FisherFace.getCurrentRecognizer()==null){
                    FisherFace.setCurrentRecognizer(new FisherFace(createFisherFaceRecognizer()));
                }else {
                    FisherFace.getCurrentRecognizer().setFish(createFisherFaceRecognizer());
                }
                FisherFace.getCurrentRecognizer().getFish().train(images, labels);
                FisherFace.getCurrentRecognizer().getFish().save("Trained.yml");
                System.out.println("training done");
            }
        }
        FisherFace.setCurrentRecognizer(new FisherFace(createFisherFaceRecognizer()));
        FisherFace.getCurrentRecognizer().getFish().load("Trained.yml");
        ORLTester(FisherFace.getCurrentRecognizer());


    }
}
*/