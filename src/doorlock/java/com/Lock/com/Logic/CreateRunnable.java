package com.Lock.com.Logic;

import com.Lock.com.Utilities.HibernateUtilities;
import com.Lock.com.Utilities.MySquare;
import com.Lock.com.Utilities.ReturnXY;
import com.Lock.com.Utilities.UsersEntity;
import com.ibm.watson.developer_cloud.visual_recognition.v3.VisualRecognition;

import java.awt.*;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.IntBuffer;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import javax.swing.*;

import static org.bytedeco.javacpp.helper.opencv_core.CV_RGB;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;
import static org.bytedeco.javacpp.opencv_core.cvPoint;
import static org.bytedeco.javacpp.opencv_face.createFisherFaceRecognizer;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgproc.cvDrawRect;
import static org.bytedeco.javacpp.opencv_imgproc.resize;
/**
 *Class to create and instance of a recognizer
 *@author Jason Bell
 **/
public class CreateRunnable implements Runnable{
    private static boolean keepRunning = false;
    private static int counter = 0;
    private static org.bytedeco.javacv.Frame frame = null;
    private static final OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();
    /**
     * Thread to create a canvas frame to take images to create a recognizer
     */
    public void run() {
        creator("Trained.yml");
        try {
            saveDatabase();
        }catch(IOException f){
            f.printStackTrace();
        }
    }
    static void creator(String saveLocation){
        counter = 0;
        keepRunning = false;
        final FrameGrabber grabber = new OpenCVFrameGrabber(0);
        final CanvasFrame canvas = new CanvasFrame("new test");
        MySquare square = new MySquare();
        JPanel newPanel = new JPanel();
        //square.setOpaque(false);
        Button takePic = new Button("Capture");
        takePic.setSize(100, 100);
        square.setVisible(true);
        takePic.setVisible(true);
        try {
            grabber.start();
            frame = grabber.grab();
            canvas.setSize(grabber.getImageWidth(), grabber.getImageHeight() + 100);
            canvas.getContentPane().add(takePic, BorderLayout.SOUTH);
            newPanel.add(square);
            canvas.getContentPane().add(square);
            square.repaint();
            canvas.pack();
            takePic.addActionListener(e-> {
               {
                    try {
                        takePicture();
                        counter++;
                        if (counter == 8) {
                            keepRunning = true;
                            canvas.dispose();
                            counter =0;
                        }
                    } catch (Exception f) {
                        f.printStackTrace();
                    }
                }
            });
            while ((frame = grabber.grab()) != null) {
                if (canvas.isVisible()) {
                    canvas.showImage(frame);
                }
                if (keepRunning) {
                    break;
                }
            }
            grabber.stop();
            grabber.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finishTraining(saveLocation);
    }

    /**
     * Takes the current frame and saves it
     */
    private static void takePicture(){
        opencv_core.IplImage finalImage;
        finalImage = converter.convert(frame);
        cvSaveImage("Profile//" + counter + "-testImage.jpg", finalImage);
    }

    /**
     * Takes the saved images, add them to a zip file to let Watson process them, then creates a recognizer object
     */
    private static void finishTraining(String saveLocation) {
        ReturnXY[] getStart;
        VisualRecognition service = new VisualRecognition(VisualRecognition.VERSION_DATE_2016_05_20);
        service.setApiKey("20c20abd5f2fd2fdc8b80cfd82008d4a453e940e");

        //creates a filter for image input

        final FilenameFilter imageFilter = (File dir, String name)-> {
            name = name.toLowerCase();
            return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
        };

        //zips the folder of images that the frame grabber creates
        String zipFile = "Watson.zip";
        String[] srcFile = {"Profile/0-testImage.jpg", "Profile/1-testImage.jpg", "Profile/2-testImage.jpg", "Profile/3-testImage.jpg", "Profile/4-testImage.jpg","Profile/5-testImage.jpg","Profile/6-testImage.jpg","Profile/7-testImage.jpg"};
        FisherFace.zip(zipFile, srcFile);
        //creates an instance of visual recognition that takes the zip as input
        getStart = FisherFace.WatsonTest(zipFile);
        //unzips the folder and crops the images
        // unzip("Profile/Watson.zip");
        //adds the images to an array
        String root = "Profile";
        File location = new File(root);
        File[] imageArray = location.listFiles(imageFilter);

        //crops the images
        counter = 0;
        if (imageArray != null) {
            for (File image : imageArray) {
                opencv_core.Mat testImage = imread(image.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
                opencv_core.Rect crop = new opencv_core.Rect((int) getStart[counter].getX(), (int) getStart[counter].getY(), (int)getStart[counter].getLength(), (int)getStart[counter].getHeight());
                opencv_core.Mat newImage = new opencv_core.Mat(testImage, crop);
                opencv_core.Size size = new opencv_core.Size(700,700);
                resize(newImage,testImage,size);
                imwrite("Profile//" + counter + "-testImage.jpg", testImage);
                counter++;
            }
        }
            //creates the recogniser with the cropped images
            imageArray = location.listFiles(imageFilter);
        if(imageArray !=null) {
            opencv_core.MatVector images = new opencv_core.MatVector(imageArray.length);
            opencv_core.Mat labels = new opencv_core.Mat(imageArray.length, 1, CV_32SC1);

            IntBuffer labelsBuffer = labels.createBuffer();
            counter = 0;

            for (File image : imageArray) {
                opencv_core.Mat imge = imread(image.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
                int label = Integer.parseInt(image.getName().split("-")[0]);
                images.put(counter, imge);
                labelsBuffer.put(counter, label);
                counter++;
            }
            double threshold = 10000.0;
            if(FisherFace.getCurrentRecognizer()==null){
                FisherFace.setCurrentRecognizer(new FisherFace(createFisherFaceRecognizer(8,threshold)));
            }else {
                FisherFace.getCurrentRecognizer().setFish(createFisherFaceRecognizer(8,threshold));
            }

            FisherFace.getCurrentRecognizer().getFish().train(images, labels);
            FisherFace.getCurrentRecognizer().getFish().save(saveLocation);
            System.out.println("training done");
        }
    }
    private static void saveDatabase()throws IOException{
        final Session session = HibernateUtilities.getSession();
        int newId = HibernateUtilities.getLastID();
        UsersEntity newUser = new UsersEntity();
        newUser.setId(newId);
        newUser.setFirstname(JOptionPane.showInputDialog(null,"Please enter your first name:"));
        newUser.setSurname(JOptionPane.showInputDialog(null,"Please enter your surname:"));
        newUser.setRecogniser(FisherFace.FacetoByte());
        try{
            session.beginTransaction();
            session.save(newUser);
            session.getTransaction().commit();
            System.out.println("Record committed to database");
        }
        catch(HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }


    }
}
