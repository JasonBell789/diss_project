package com.Lock.com.Logic;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.bytedeco.javacpp.opencv_face.createFisherFaceRecognizer;

import arduino.Arduino;
import com.Lock.ProjectGui;
import com.Lock.com.Utilities.HibernateUtilities;
import com.Lock.com.Utilities.PortScanner;
import com.Lock.com.Utilities.ReturnXY;
import com.Lock.com.Utilities.UsersEntity;
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;

import com.ibm.watson.developer_cloud.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.DetectFacesOptions;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.DetectedFaces;

import javax.swing.*;
/**
 * Class containing utility methods and the button logic of the main GUI
 *@author Jason Bell
 **/
public class FisherFace {
    private static FisherFace currentRecognizer;
    private FaceRecognizer fish;
    private static String[] commands = {"1","2"}; // commands that adruino can recognize
    private static String port = (new PortScanner()).getActivePort(); // <--- get the port
    private static Arduino AdruinoCon = new Arduino(port,9600);
    private static boolean isOn = false; // state of the led
    /**
     *
     * @param fishface A FaceRecognizer object
     */
    public FisherFace(FaceRecognizer fishface) {
        this.fish = fishface;
    }

    /**
     * A getter for a FaceRecognizer object
     * @return returns the face recognizer of a created Fisherface object
     */
    FaceRecognizer getFish(){
        return fish;
    }

    /**
     * A Setter for a FaceReognizer object
     * @param newFish A FaceRecognizer Object
     */
   void setFish(FaceRecognizer newFish){
        fish = newFish;
    }

    /**
     * Getter for a static instance of the FisherFace object
     * @return Returns the Current FisherFace object, a static recognizer that the programs loads in or overwrites for testing
     */
    static FisherFace getCurrentRecognizer(){
        return currentRecognizer;
    }

    /**
     * Setter for the Static instance of the FisherFace Object
     * @param newfish A FaceRecognizer object
     */
    static void setCurrentRecognizer(FisherFace newfish) {
        currentRecognizer = newfish;
    }

    /**
     * Starts a CreateRunnable thread to create or overwrite the static FisherFace Object
     */
    public void createTrainer() {
        SwingUtilities.invokeLater(() -> {
                Thread t = new Thread(new CreateRunnable());
                t.start();
        });

    }
    public void createTempTrainer(){
        SwingUtilities.invokeLater(() -> {
                Thread t = new Thread(new Temp_create());
                t.start();
        });
    }

    /**
     * Reads in the Trained.yml file into the static FisherFace Object
     */
    public  void loadTrainer() {
        final List userList = HibernateUtilities.getUsers();
        JPanel panel = new JPanel();
        final JComboBox comboBox = new JComboBox();
        final JDialog dialog = new JDialog();
        JButton okButton = new JButton("Select user");
        final double threshhold = 10000.0;
        for (int i = 0; i < userList.size(); i++) {
            UsersEntity nextUser = (UsersEntity) userList.get(i);
            comboBox.addItem(nextUser.getId() + " " + nextUser.getFirstname() + " " + nextUser.getSurname());

            okButton.addActionListener(e -> {
                    try {
                        int num = (comboBox.getSelectedIndex());
                        UsersEntity choosenUser = (UsersEntity)userList.get(num);
                        BytetoFace(choosenUser.getRecogniser());
                        fish = createFisherFaceRecognizer(0,threshhold);
                        currentRecognizer = new FisherFace(fish);
                        fish.load("Trained.yml");
                        System.out.println("trainer loaded");
                        dialog.dispose();
                    } catch (Exception p) {
                        p.printStackTrace();
                    }
            });
        }
        panel.setLayout(new FlowLayout());
        dialog.setTitle("Please select your user account");
        panel.add(comboBox,Component.TOP_ALIGNMENT);
        panel.add(okButton,Component.BOTTOM_ALIGNMENT);
        dialog.getContentPane().add(panel);
        dialog.pack();
        dialog.setVisible(true);
    }

    /**
     * Starts a TestRunnable thread to test a face against the Recognizer
     */
    public  void testImage() {
        SwingUtilities.invokeLater(() -> {
                Thread t = new Thread(new TestRunnable());
                t.start();
        });
    }
    public  void tempTest(){
        SwingUtilities.invokeLater(() -> {
                Thread t = new Thread(new Temp_Test());
                t.start();
        });
    }

    public static void locker(){
        isOn = !isOn; // if the current state is TRUE we set it to FALSE
        int commandIndex = (isOn) ? 1 : 0; // false = 0; true = 1
        PortScanner.lock(commands[commandIndex]);
    }

    /**
     * Utility to zip Files for Watson to use
     * @param zipFile The name of the outputted .zip file
     * @param srcFile An array of strings representing the files to be zipped
     */
    static void zip(String zipFile,String[] srcFile){
        try{
            byte[] buffer = new byte [1024];
            FileOutputStream output = new FileOutputStream(zipFile);
            ZipOutputStream zipOutput = new ZipOutputStream(output);
            for(String currentFile:srcFile){
                File fileToZip = new File(currentFile);
                FileInputStream input = new FileInputStream(fileToZip);
                ZipEntry zipEntry = new ZipEntry((fileToZip.getName()));
                zipOutput.putNextEntry(zipEntry);

                int length;
                while ((length = input.read(buffer)) >= 0) {
                    zipOutput.write(buffer, 0, length);
                }
                input.close();
            }
            zipOutput.close();
            output.close();
        }catch(IOException e) {
            System.out.println("Error - File not Found "+e);
        }

    }

    /**
     * Makes a request to Watson to classify an image/set of images
     * @param imageFile Image or .zip file of images to be classified
     * @return and instance of ReturnXY, containing the x and y location of the start of a found face
     */
    static ReturnXY[] WatsonTest(String imageFile){
        double x;
        double y;
        double height;
        double width;
        ReturnXY[] newXY = null;
        VisualRecognition service = ProjectGui.getService();
        System.out.println("Classify an image");
        try{
            DetectFacesOptions options = new DetectFacesOptions.Builder()
                    .imagesFile(new File (imageFile))
                    .build();

            DetectedFaces result = service.detectFaces(options).execute();
            System.out.println(result);
            newXY = new ReturnXY[result.getImages().size()];
            for(int i=0;i<result.getImages().size();i++) {
                x = result.getImages().get(i).getFaces().get(0).getFaceLocation().getLeft();
                y = result.getImages().get(i).getFaces().get(0).getFaceLocation().getTop();
                height = result.getImages().get(i).getFaces().get(0).getFaceLocation().getHeight();
                width = result.getImages().get(i).getFaces().get(0).getFaceLocation().getWidth();
                newXY[i] = new ReturnXY(x,y,width,height);
            }
        } catch(FileNotFoundException e) {
            System.out.println("Error - File not found " + e);
        }
        return newXY;
    }
    static byte[] FacetoByte() throws IOException{
        Path path = Paths.get("Trained.yml");
        return Files.readAllBytes(path);

    }
    private static void BytetoFace(byte[] bytes)throws IOException, ClassNotFoundException{

        File file = new File("Trained.yml");
        FileOutputStream outStream = new FileOutputStream(file);
        outStream.write(bytes);
        outStream.close();

    }
}