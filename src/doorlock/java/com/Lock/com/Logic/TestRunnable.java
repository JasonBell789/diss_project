package com.Lock.com.Logic;

import java.awt.*;

import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgproc.resize;

import arduino.Arduino;
import com.Lock.com.Utilities.PortScanner;
import com.Lock.com.Utilities.ReturnXY;
import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.*;

import javax.sound.sampled.Port;


/**
 *Class to perform a test against a recognizer
 *@author Jason Bell
 **/
public class TestRunnable implements Runnable{

        private static boolean keepRunning = false;
        private static opencv_core.IplImage finalImage;
        private static String[] commands = {"1","2"}; // commands that adruino can recognize
        private static String port = (new PortScanner()).getActivePort(); // <--- get the port
        private static Arduino AdruinoCon = new Arduino(port,9600);
        private static boolean isOn = false; // state of the led


    /**
     * Thread to create a canvas frame to take an image and test it against a classifier
     */
    public void run() {
        createTest();
        }
    static void createTest(){
        keepRunning = false;
        FrameGrabber grabber = new OpenCVFrameGrabber(0);
        final CanvasFrame canvas = new CanvasFrame("new test");
        Button takePic = new Button("Capture");
        takePic.setSize(100, 100);
        takePic.setVisible(true);
        org.bytedeco.javacv.Frame frame;
        try {
            grabber.start();
            canvas.setSize(grabber.getImageWidth(), grabber.getImageHeight() + 100);
            canvas.getContentPane().add(takePic, BorderLayout.SOUTH);
               /* JLabel box = new MySquare(250,250,300,300);
                box.setSize(1000,1000);
                box.setOpaque(false);
                box.repaint();
                box.setVisible(true);
                canvas.getLayeredPane().add(box,BorderLayout.CENTER);*/
            takePic.addActionListener(e -> {
                    canvas.dispose();
                    keepRunning = true;
            });
            while ((frame = grabber.grab()) != null) {
                if (canvas.isVisible()) {
                    canvas.showImage(frame);
                }
                if(keepRunning){break;}
            }
            grabber.stop();
            grabber.release();
            OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();
            finalImage = converter.convert(frame);
            runTest(FisherFace.getCurrentRecognizer());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Takes the image and tests it against a recognizer
     * @param recognizer A FisherFace object
     */
    private static void runTest(FisherFace recognizer){
            try{
                cvSaveImage("testImage.jpg", finalImage);
                ReturnXY[] getstart;
                getstart = FisherFace.WatsonTest("testImage.jpg");

                IntPointer label = new IntPointer(1);
                DoublePointer confidence = new DoublePointer(1);

                opencv_core.Mat testImage = imread("testImage.jpg", CV_LOAD_IMAGE_GRAYSCALE);
                opencv_core.Rect crop = new opencv_core.Rect((int) getstart[0].getX(), (int) getstart[0].getY(), (int)getstart[0].getLength(), (int)getstart[0].getHeight());
                opencv_core.Mat newImage = new opencv_core.Mat(testImage, crop);
                opencv_core.Size size = new opencv_core.Size(700,700);
                resize(newImage,testImage,size);
                imwrite("testImage.jpg", testImage);
                opencv_core.Mat tester = imread("testImage.jpg", CV_LOAD_IMAGE_GRAYSCALE);
                recognizer.getFish().predict(tester,label,confidence);
                int predictedLabel = label.get(0);
                double predictedConfidence = confidence.get(0);
                System.out.println("Predicted label: " + predictedLabel + " " +
                        "Predicted Confidence: " + predictedConfidence);
                if(predictedLabel >=0){
                    isOn = !isOn; // if the current state is TRUE we set it to FALSE
                    int commandIndex = (isOn) ? 1 : 0; // false = 0; true = 1
                    PortScanner.lock(commands[commandIndex]);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
}