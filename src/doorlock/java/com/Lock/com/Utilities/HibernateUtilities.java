package com.Lock.com.Utilities;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;


public class HibernateUtilities {

    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }
    public static int getLastID(){
        int newid;
        Query query = getSession().createQuery("from UsersEntity ");
        List allUsers = query.list();
        if(allUsers.size()==0){
            newid = 1;
        }else {
            int j = allUsers.size();
            newid = j+1;
        }
        return newid;
    }
    public  static List getUsers(){
        Query query = getSession().createQuery("from UsersEntity ");
        return query.list();
    }
}
