package com.Lock.com.Utilities;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Utility class to draw a square over a frame
 * @author Jason Bell
 */
public class MySquare extends JComponent {

    public void paint(Graphics g){
        Shape rect;
        Graphics2D graph2 = (Graphics2D)g;
        graph2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        rect = new Rectangle2D.Float(50,50,100,100);


        graph2.setPaint(Color.BLACK);
        graph2.fill(rect);
        graph2.draw(rect);

    }

}
