package com.Lock.com.Utilities;

import arduino.Arduino;
import gnu.io.CommPortIdentifier;
import java.util.Enumeration;


public class PortScanner{
    private String activePort;
     private static String port = (new PortScanner()).getActivePort(); // <--- get the port
    private static Arduino AdruinoCon = new Arduino(port,9600);
    private static boolean isOn = false; // state of the led



    public PortScanner(){
        CommPortIdentifier serialPortId;

        Enumeration enumComm;

        enumComm = CommPortIdentifier.getPortIdentifiers(); // get active ports

        /*
            go through ports and identify the serial
         */
        while(enumComm.hasMoreElements())
        {
            serialPortId = (CommPortIdentifier) enumComm.nextElement(); // get next one
            if(serialPortId.getPortType() == CommPortIdentifier.PORT_SERIAL) // check if is serial type
            {
                this.activePort = (String) serialPortId.getName();
            }
        }
    }
    public static void lock(String command){
        AdruinoCon.openConnection();
        AdruinoCon.serialWrite(command);
    }

    public String getActivePort(){
        return this.activePort;
    }
    // JavaFX main function containing the logic


}
