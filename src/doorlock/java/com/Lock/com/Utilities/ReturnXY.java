package com.Lock.com.Utilities;

/**
 * @author Jason Bell
 * Class to create a utility object so Watson can return two variables
 */
public class ReturnXY {
    private double x,y,width,height;

    /**
     * Constructor to create an instance of the class
     * @param returnX double that contains the x variable of an image
     * @param returnY double the contains the y variable of an image
     */
    public ReturnXY(double returnX,double returnY,double returnWidth,double returnHeight){
        this.x = returnX;
        this.y = returnY;
        this.width = returnWidth;
        this.height = returnHeight;

    }

    /**
     * Getter for variable x
     * @return x
     */
    public double getX(){
        return x;
    }

    /**
     * Getter for variable y
     * @return Y
     */
    public double getY(){
        return y;
    }

    public double getLength() {return width;}

    public double getHeight() {return  height;}

    /*public void setX(double X){this.x = X;}

    public void setY(double Y){this.y = Y;}

    public  void setWidth(double width){this.width = width;}

    public void setHeight(double height){this.height = height;}
*/

}
