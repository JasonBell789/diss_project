package com.Lock.com.Utilities;

import javax.persistence.*;
import java.util.Arrays;


@Entity
@Table(name = "users", schema = "public", catalog = "Diss")
public class UsersEntity {
    private int id;
    private String firstname;
    private String surname;
    private byte[] recogniser;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "firstname")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "recogniser")
    public byte[] getRecogniser() {
        return recogniser;
    }

    public void setRecogniser(byte[] recogniser) {
        this.recogniser = recogniser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (id != that.id) return false;
        if (firstname != null ? !firstname.equals(that.firstname) : that.firstname != null) return false;
        if (surname != null ? !surname.equals(that.surname) : that.surname != null) return false;
        if(!Arrays.equals(recogniser, that.recogniser)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(recogniser);
        return result;
    }
}
